#pragma once

#ifndef CONFIG_BOOT_ORIGIN
# error CONFIG_BOOT_ORIGIN should be defined in an INI file
#endif

#include <stdint.h>

namespace porpoise { namespace boot {
    extern "C" [[noreturn]] void boot_kernel(uint64_t dtb, uint64_t id);
}} // porpoise::boot
