#pragma once

#include <limits.h>
#include <stdint.h>
#include <stddef.h>
#include <type_traits>

#include <etl/algorithm.h>
#include <etl/array.h>
#include <etl/bitset.h>
#include <etl/container.h>
#include <etl/mutex.h>

#include <porpoise/capability.hpp>
#include <porpoise/lib/container/sorted-vector.hpp>
#include <porpoise/mem/internal/block-bitset.hpp>

#define PORPOISE_MEM_POINTER_BITS (static_cast<uint_least64_t>(sizeof(uintptr_t)*CHAR_BIT))

#ifndef CONFIG_MEM_HEAP_BLOCKS_PER_BIN
    #error CONFIG_MEM_HEAP_BLOCKS_PER_BIN is not defined
#endif

#ifndef CONFIG_MEM_HEAP_BIN_BLOCK_SIZES
    #error CONFIG_MEM_HEAP_BIN_BLOCK_SIZES is not defined
#endif

namespace porpoise { namespace mem {
    enum class oom_behaviour
    {
        abort,
        return_null,
        default_ = abort
    };

    template <class T>
    bool is_address_aligned(T address, size_t boundary)
    {
        static_assert(std::is_integral<T>::value);
        return address%boundary == 0;
    }

    template <class T, class U>
    bool is_address_aligned(T address)
    {
        return is_address_aligned(address, sizeof(U));
    }

    template <class T>
    T get_aligned_address(T address, size_t boundary)
    {
        static_assert(std::is_integral<T>::value);
        auto x = address%boundary;
        if (x)
        {
            return address - x + boundary;
        }

        return address;
    }

    template <class T>
    ptrdiff_t make_address_aligned(T& address, size_t boundary)
    {
        static_assert(std::is_integral<T>::value);
        auto next = get_aligned_address(address, boundary);
        auto diff = static_cast<intmax_t>(next) - static_cast<intmax_t>(address);

        {
            // Check overflow in debug mode.
            auto abs_diff = static_cast<size_t>(diff < 0 ? -diff : diff);
            PORPOISE_ASSERT_LESS(abs_diff, static_cast<size_t>(PTRDIFF_MAX));
        }

        address = next;
        return static_cast<ptrdiff_t>(diff);
    }

    /// Kernel heap manager.
    struct heap
    {
        static constexpr void* OUT_OF_MEMORY = nullptr;
        static constexpr void* MOVE_BIN = nullptr;
        static constexpr size_t PAGE_SIZE = 4096;
        static constexpr size_t BIN_BLOCK_SIZES[] = CONFIG_MEM_HEAP_BIN_BLOCK_SIZES;
        static constexpr size_t NUM_HEAP_BINS = etl::size(BIN_BLOCK_SIZES);
        static constexpr size_t BLOCKS_PER_BIN = CONFIG_MEM_HEAP_BLOCKS_PER_BIN;

        static_assert(NUM_HEAP_BINS > 0);

        /// Object containing statistics about the heap.
        struct statistics {
            size_t num_bins;         ///< The number of bins.
            size_t allocations;      ///< The number of calls to heap::allocate().
            size_t frees;            ///< The number of calls to heap::deallocate().
            size_t total_bytes;      ///< The total number of bytes of memory managed by the heap.
            size_t bytes_allocated;  ///< The number of bytes allocated in the heap.
            size_t total_blocks;     ///< The total number of blocks managed by the heap.
            size_t blocks_allocated; ///< The total number of blocks allocated in the heap.
        };

        /// Get a reference to the heap.
        static heap& get(const heap_capability& cap);

        /// Get the address of the end of the heap.
        static uintptr_t end();

        /// Initialise the heap at address 'start'.
        void init(uintptr_t start, const init_capability& cap);

        /// Map an immovable piece of memory.
        void map(void* p, size_t size);

        /// Allocate a memory region. If memory cannot be allocated, abort or return null depending on oom_behaviour.
        void* allocate(size_t bytes, oom_behaviour behaviour = oom_behaviour::default_);

        /// Reallocate a memory region.
        /// 1. If p is nullptr, behaves like allocate(bytes, behaviour).
        /// 2. Otherwise, if bytes is 0, behaves like deallocate(p);
        /// 3. Otherwise, if p can be resized in place, returns p.
        /// 4. Otherwise, if p can be moved to a new location, returns a pointer to the new memory (caller deallocates 
        ///    p).
        /// 5. Otherwise, the behaviour depends on the value of 'behaviour'.
        /// \warning This function differs from realloc(3) in that the caller is responsible for freeing 'p' when new
        /// memory is allocated.
        void* reallocate(void* p, size_t bytes, oom_behaviour behaviour = oom_behaviour::default_);

        /// Deallocate memory region.
        void deallocate(void* p);

        /// Compute heap statistics.
        void get_statistics(statistics& stats);
    private:
        struct heap_bin {
            heap_bin(uintptr_t start, size_t blksize);

            void map(void* p, size_t bytes);

            bool owns(void* p) const;

            void* allocate(size_t bytes);

            void* reallocate(void* p, size_t bytes);

            void deallocate(void* p);

            size_t blocks_total_size() const;

            size_t block_size() const;

            size_t total_blocks() const;

            size_t bytes_allocated() const;

            size_t blocks_allocated() const;
        private:
            uintptr_t _start;
            size_t _blksize;
            internal::block_bitset<BLOCKS_PER_BIN> _bitset;
        };

        static heap _inst;
        etl::array<heap_bin*, NUM_HEAP_BINS> _bins;
        etl::mutex _lock;
        uintptr_t _start;
        uintptr_t _end;
        size_t _allocations;
        size_t _frees;

        heap() = default;

        void* handle_oom(oom_behaviour behaviour);
    };
}} // porpoise::mem
