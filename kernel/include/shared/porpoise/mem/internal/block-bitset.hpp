#pragma once

#include <etl/bitset.h>

namespace porpoise { namespace mem { namespace internal {
    template <size_t N>
    struct block_bitset {
        void set(size_t i, size_t n)
        {
            for (; n; i++, n--) {
                _bitset.set(i, true);
            }
        }

        void clear(size_t i, size_t n)
        {
            for (; n; i++, n--) {
                _bitset.set(i, false);
            }
        }

        bool take(size_t& taken)
        {
            for (size_t i = 0; i < _bitset.size(); i++) {
                if (!_bitset[i]) {
                    taken = i;
                    return true;
                }
            }

            return false;
        }

        void give_back(size_t i)
        {
            _bitset[i] = false;
        }

        size_t count() const
        {
            return _bitset.count();
        }

        size_t size() const
        {
            return N;
        }
    private:
        etl::bitset<N> _bitset;
    };
}}} // porpoise::mem::internal
