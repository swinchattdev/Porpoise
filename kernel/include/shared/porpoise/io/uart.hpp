#pragma once

#include <stdint.h>

namespace porpoise { namespace io {
    struct uart {
        static uart* init();

        void put(uint8_t value);

        uint8_t get();
    private:
        static uart _instance;

        uart() = default;
    };
}} // porpoise::io
