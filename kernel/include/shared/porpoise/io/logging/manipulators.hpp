#pragma once

#include <limits.h>
#include <stdint.h>

#include <porpoise/io/logging/log.hpp>

namespace porpoise { namespace io { namespace logging {
    struct reset : manipulator
    {
        void operator()(Log& log_) override
        {
            log_.base(10);
            log_.prefix(false);
            log_.hexupper(false);
            log_.boolalpha(false);
            log_.field_width(0);
            log_.fill_char(' ');
        }
    };

    struct set_width : manipulator
    {
        explicit set_width(uint8_t value) : _value(value) {}
        void operator()(Log& log_) override {log_.field_width(_value);}
    private:
        uint8_t _value;
    };

    struct set_fill : manipulator
    {
        explicit set_fill(char value) : _value(value) {}
        void operator()(Log& log_) override {log_.fill_char(_value);}
    private:
        char _value;
    };

    struct set_base : manipulator
    {
        explicit set_base(uint8_t value) : _value(value) {}
        void operator()(Log& log_) override {log_.base(_value);}
    private:
        uint8_t _value;
    };

    struct show_prefix : manipulator
    {
        explicit show_prefix(bool value) : _value(value) {}
        void operator()(Log& log_) override {log_.prefix(_value);}
    private:
        bool _value;
    };

    struct boolalpha : manipulator
    {
        explicit boolalpha(bool value) : _value(value) {}
        void operator()(Log& log_) override {log_.boolalpha(_value);}
    private:
        bool _value;
    };

    struct hexupper : manipulator
    {
        explicit hexupper(bool value) : _value(value) {}
        void operator()(Log& log_) override {log_.hexupper(_value);}
    private:
        bool _value;
    };
}}} // porpoise::io::logging

porpoise::io::logging::Log& operator<<(porpoise::io::logging::Log& log_, nullptr_t);
porpoise::io::logging::Log& operator<<(porpoise::io::logging::Log& log_, void*);
porpoise::io::logging::Log& operator<<(porpoise::io::logging::Log& log_, char c);
porpoise::io::logging::Log& operator<<(porpoise::io::logging::Log& log_, const char* s);
porpoise::io::logging::Log& operator<<(porpoise::io::logging::Log& log_, int8_t number);
porpoise::io::logging::Log& operator<<(porpoise::io::logging::Log& log_, int16_t number);
porpoise::io::logging::Log& operator<<(porpoise::io::logging::Log& log_, int32_t number);
porpoise::io::logging::Log& operator<<(porpoise::io::logging::Log& log_, int64_t number);
porpoise::io::logging::Log& operator<<(porpoise::io::logging::Log& log_, uint8_t number);
porpoise::io::logging::Log& operator<<(porpoise::io::logging::Log& log_, uint16_t number);
porpoise::io::logging::Log& operator<<(porpoise::io::logging::Log& log_, uint32_t number);
porpoise::io::logging::Log& operator<<(porpoise::io::logging::Log& log_, uint64_t number);
porpoise::io::logging::Log& operator<<(porpoise::io::logging::Log& log_, porpoise::io::logging::reset manip);
porpoise::io::logging::Log& operator<<(porpoise::io::logging::Log& log_, porpoise::io::logging::set_width manip);
porpoise::io::logging::Log& operator<<(porpoise::io::logging::Log& log_, porpoise::io::logging::set_fill manip);
porpoise::io::logging::Log& operator<<(porpoise::io::logging::Log& log_, porpoise::io::logging::set_base manip);
porpoise::io::logging::Log& operator<<(porpoise::io::logging::Log& log_, porpoise::io::logging::show_prefix manip);
porpoise::io::logging::Log& operator<<(porpoise::io::logging::Log& log_, porpoise::io::logging::boolalpha manip);
porpoise::io::logging::Log& operator<<(porpoise::io::logging::Log& log_, porpoise::io::logging::hexupper manip);
