#pragma once

#include <stdint.h>
#include <stddef.h>

#include <etl/mutex.h>

#ifndef PORPOISE_LOG_GROUP
# define PORPOISE_LOG_GROUP "dflt"
#endif

#define _PORPOISE_LOG_COMMON(LEVEL, ...)                   \
    do {                                                   \
        auto __log = ::porpoise::io::logging::Log::LEVEL();\
        if (sizeof(PORPOISE_LOG_GROUP)) {                  \
            __log << PORPOISE_LOG_GROUP << ": ";           \
        }                                                  \
        __log << __VA_ARGS__;                              \
    } while (0)

#if CONFIG_LOG_MIN_LEVEL > 0
# define PORPOISE_LOG_TRACE(...)
#else
# define PORPOISE_LOG_TRACE(...) _PORPOISE_LOG_COMMON(trace, __VA_ARGS__)
#endif

#if CONFIG_LOG_MIN_LEVEL > 1
# define PORPOISE_LOG_DEBUG(...)
#else
# define PORPOISE_LOG_DEBUG(...) _PORPOISE_LOG_COMMON(debug, __VA_ARGS__)
#endif

#if CONFIG_LOG_MIN_LEVEL > 2
# define PORPOISE_LOG_INFO(...)
#else
# define PORPOISE_LOG_INFO(...)  _PORPOISE_LOG_COMMON(info, __VA_ARGS__)
#endif

#define PORPOISE_LOG_WARN(...)  _PORPOISE_LOG_COMMON(warn, __VA_ARGS__)
#define PORPOISE_LOG_ERROR(...) _PORPOISE_LOG_COMMON(error, __VA_ARGS__)

namespace porpoise { namespace io { namespace logging {
    /// Log level.
    enum class log_level : uint8_t
    {
        trace,
        debug,
        info,
        warn,
        error
    };

    /// Log sink, e.g. serial device, a file or the screen.
    struct log_sink
    {
        /// Emit a string verbatim if the Log level is greater than or equal to the sink's Log-level.
        virtual void emit(log_level level, const char* event) = 0;

        /// Emit a character verbatim if the Log level is greater than or equal to the sink's Log-level.
        virtual void emit(log_level level, char c) = 0;
    };

    struct Log;

    struct manipulator
    {
        virtual void operator()(Log& log_) = 0;
    };

    struct Log
    {
        static constexpr int MAX_SINK = 16;

        static Log trace();
        
        static Log debug();
        
        static Log info();
        
        static Log warn();
        
        static Log error();

        static log_level minimum_level();

        static void minimum_level(log_level next);

        static int num_sinks();

        static bool add_sink(log_sink* sink);

        Log(Log&& other);

        void operator=(Log&& other);

        virtual ~Log();

        void emit(char c);

        void emit(const char* string);

        void emit(bool value);

        void emit(intmax_t number);

        void emit(uintmax_t number);

        void flush();

        uint8_t field_width() const;

        char fill_char() const;

        uint8_t base() const;

        bool prefix() const;

        bool boolalpha() const;

        bool hexupper() const;

        void field_width(uint8_t next);

        void fill_char(char next);

        void base(uint8_t next);

        void prefix(bool next);

        void boolalpha(bool next);

        void hexupper(bool next);

        Log(const Log&) = delete;

        void operator=(const Log&) = delete;
    protected:
        explicit Log(log_level level);

    private:
        static log_level _minimum_level;
        static log_sink* _sinks[MAX_SINK];
        static int _num_sinks;
        static etl::mutex _sink_lock;

        static Log get(log_level level, const char* lvlstr);

        log_level _current_level;
        uint8_t   _field_width;
        char      _fill_char;
        uint8_t   _base;
        bool      _prefix;
        bool      _boolalpha;
        bool      _hexupper;
        bool      _is_active;

        bool is_active_instance() const;

        void internal_emit(char c);

        void internal_emit(const char* s);
    };

}}} // porpoise::io::logging
