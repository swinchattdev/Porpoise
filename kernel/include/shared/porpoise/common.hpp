#pragma once

#define PORPOISE_UNUSED(X) ((void)(X))
#define MIN(a, b)          ((a) < (b) ? (a) : (b))
#define MAX(a, b)          ((a) > (b) ? (a) : (b))
#define MAX3(a, b)         MAX(MAX(a, b), c)
#define MIN3(a, b)         MIN(MIN(a, b), c)
