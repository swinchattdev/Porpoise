#pragma once

namespace porpoise { namespace lib { namespace sync {
    template <class Mutex>
    struct lock_guard {
        lock_guard(Mutex& mutex) : _mutex(&mutex)
        {
            if (_mutex) {
                _mutex->lock();
            }
        }

        lock_guard(const lock_guard<Mutex>& other)
        {
            _mutex = other._mutex;
            other._mutex = nullptr;
        }

        lock_guard(lock_guard<Mutex>&& other)
        {
            _mutex = other._mutex;
            other._mutex = nullptr;
        }

        ~lock_guard()
        {
            if (_mutex) {
                _mutex->unlock();
            }
        }
    private:
        Mutex* _mutex;
    };

    template <class Mutex>
    lock_guard<Mutex> make_lock_guard(Mutex& mutex)
    {
        return lock_guard<Mutex>(mutex);
    }
}}}
