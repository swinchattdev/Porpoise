#pragma once

#include <porpoise/boot-platform.hpp>

#ifndef __ASM_SOURCE__
#include <stdint.h>

#include <porpoise/capability.hpp>

namespace porpoise {
    void cpu_main(int id, const heap_capability& heap_cap);
}

namespace porpoise { namespace boot {
    void start_cpu(int id, void(* fn)());
}} // porpoise::boot

#endif // ! __ASM_SOURCE__
