#pragma once

#include <stdint.h>
#include <porpoise/time/timespan.hpp>

namespace porpoise { namespace time {
    void delay(const timespan& t);
}} // porpose::time
