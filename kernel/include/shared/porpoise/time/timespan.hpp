#pragma once

#include <stdint.h>

#define MICROS_TO_CYCLES(MICROS, HERTZ) ((MICROS)*(HERTZ)/1000000)

#define MILLIS_TO_CYCLES(MILLIS, HERTZ) ((MILLIS)*(HERTZ)/1000)

#define CYCLES_TO_MICROS(CYCLES, HERTZ) ((CYCLES)*1000000/(HERTZ))

#define CYCLES_TO_MILLIS(CYCLES, HERTZ) ((CYCLES)*1000/(HERTZ))

namespace porpoise { namespace time {
    struct timespan {
        static constexpr uintmax_t DEFAULT_CPU_HERTZ = 1000000000; // 1 GHz.

        static void cpu_hertz(uintmax_t next);
    
        static uintmax_t cpu_hertz();

        static timespan read_timestamp_counter();

        static timespan cycles(uintmax_t value);
        
        static timespan micros(uintmax_t value);
        
        static timespan millis(uintmax_t value);

        explicit timespan(uintmax_t cycles);

        uintmax_t cycles() const;
        
        uintmax_t micros() const;
        
        uintmax_t millis() const;
    private:
        static uintmax_t _cpu_hertz;
        uintmax_t _cycles;
    };

    timespan operator-(const timespan& a, const timespan& b);
    timespan operator+(const timespan& a, const timespan& b);
}} // porpoise::time
