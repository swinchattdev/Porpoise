set(KERNEL_TARGET porpoise-kernel)
add_executable(${KERNEL_TARGET})

# Compiler & linker options.
set(CMAKE_ASM_COMPILER ${MACHINE_TARGET_TRIPLE}-g++)
set(CMAKE_CXX_COMPILER ${MACHINE_TARGET_TRIPLE}-g++)
set(CMAKE_LINKER ${MACHINE_TARGET_TRIPLE}-g++)
target_compile_options(
    ${KERNEL_TARGET}
        PRIVATE
            -ffreestanding -nostdlib -fno-exceptions -fno-rtti -fno-stack-protector -Wall -Wextra
)
target_link_options(
    ${KERNEL_TARGET}
        PRIVATE
            -T "${CMAKE_CURRENT_SOURCE_DIR}/ld/${MACHINE}${MACHINE_VERSION}.ld"
            -ffreestanding -nostdlib -fno-exceptions -fno-rtti -fno-stack-protector -Wall -Wextra
)

# Add configuration files
execute_process(
    COMMAND "${PROJECT_SOURCE_DIR}/tools/cat-config" "${CMAKE_CURRENT_SOURCE_DIR}/config/generic.ini"
    WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
    OUTPUT_VARIABLE GENERIC_CONFIG_DEFINES
)
execute_process(
    COMMAND "${PROJECT_SOURCE_DIR}/tools/cat-config-if-exists" "${CMAKE_CURRENT_SOURCE_DIR}/config/machine-specific/${MACHINE}${MACHINE_VERSION}.ini"
    WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
    OUTPUT_VARIABLE MACHINE_CONFIG_DEFINES
)
add_definitions(${GENERIC_CONFIG_DEFINES})
add_definitions(${MACHINE_CONFIG_DEFINES})
add_definitions(${MACHINE_CMDLINE_DEFINES})
add_definitions(-DMACHINE="${MACHINE}" -DMACHINE_VERSION=${MACHINE_VERSION} -DMACHINE_TARGET_TRIPLE="${MACHINE_TARGET_TRIPLE}")

# Add include & source dirs
target_include_directories(
    ${KERNEL_TARGET}
        PRIVATE
            ./include/
            ./include/${MACHINE}
            ./include/${MACHINE}/lib
            ./include/${MACHINE}${MACHINE_VERSION}
            ./include/${MACHINE}${MACHINE_VERSION}/lib
            ./include/shared
            ./include/shared/lib
)
add_subdirectory(source)

# Libraries
target_link_libraries(
    ${KERNEL_TARGET}
        PRIVATE
            gcc
)
