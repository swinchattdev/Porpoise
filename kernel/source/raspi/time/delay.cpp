#include <stdint.h>
#include <porpoise/time/delay.hpp>

#define CNTPCT(X) asm volatile("mrs %0, cntpct_el0":"=r"(X))

namespace porpoise { namespace time {
    void delay(const timespan& t)
    {
        uint64_t curr;
        CNTPCT(curr);
        uint64_t max = curr + t.cycles();
        while (curr < max) {;
            CNTPCT(curr);
        }
    }
}} // porpose::time
