#include <etl/algorithm.h>

extern "C" {
    void* memset(void* p, int value, size_t num)
    {
        while (num --> 0) {
            reinterpret_cast<uint8_t*>(p)[num] = static_cast<uint8_t>(value);
        }

        return p;
    }

    static void directional_copy(uint8_t* dst, const uint8_t* src, size_t num, int direction)
    {
        while (num --> 0) {
            *dst = *src;
            dst += direction;
            src += direction;
        }
    }

    void* memcpy(void* dst, const void* src, size_t num)
    {
        auto pdst = reinterpret_cast<uint8_t*>(dst);
        auto psrc = reinterpret_cast<const uint8_t*>(src);
        directional_copy(pdst, psrc, num, 1);
        return dst;
    }

    void* memmove(void* dst, const void* src, size_t num)
    {
        auto pdst = reinterpret_cast<uint8_t*>(dst);
        auto psrc = reinterpret_cast<const uint8_t*>(src);
        if (pdst + num <= psrc || psrc + num <= pdst) {
            directional_copy(pdst, psrc, num, 1);
        } else {
            directional_copy(pdst, psrc, num, -1);
        }

        return dst;
    }
}