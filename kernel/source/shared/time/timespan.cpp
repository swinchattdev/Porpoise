#include <porpoise/time/timespan.hpp>

namespace porpoise { namespace time {
    uintmax_t timespan::_cpu_hertz = DEFAULT_CPU_HERTZ;

    void timespan::cpu_hertz(uintmax_t next)
    {
        _cpu_hertz = next;
    }
    
    uintmax_t timespan::cpu_hertz()
    {
        return _cpu_hertz;
    }

    timespan timespan::cycles(uintmax_t value)
    {
        return timespan(value);
    }
        
    timespan timespan::micros(uintmax_t value)
    {
        return timespan(MICROS_TO_CYCLES(value, _cpu_hertz));
    }
        
    timespan timespan::millis(uintmax_t value)
    {
        return timespan(MILLIS_TO_CYCLES(value, _cpu_hertz));
    }

    timespan::timespan(uintmax_t cycles) : _cycles(cycles)
    {
    }

    uintmax_t timespan::cycles() const
    {
        return _cycles;
    }
        
    uintmax_t timespan::micros() const
    {
        return CYCLES_TO_MICROS(_cycles, _cpu_hertz);
    }
        
    uintmax_t timespan::millis() const
    {
        return CYCLES_TO_MILLIS(_cycles, _cpu_hertz);
    }

    timespan operator+(const timespan& a, const timespan& b)
    {
        return timespan::cycles(a.cycles() + b.cycles());
    }

    timespan operator-(const timespan& a, const timespan& b)
    {
        return timespan::cycles(a.cycles() - b.cycles());
    }
}} // porpoise::time
