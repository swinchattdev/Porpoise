#include <stdint.h>
#include <stddef.h>

#define PORPOISE_LOG_GROUP "heapf"

#include <porpoise/common.hpp>
#include <porpoise/assert.hpp>
#include <porpoise/mem/heap.hpp>

extern uint8_t __kernel_end__[];

namespace porpoise { namespace mem {
    heap::heap_bin::heap_bin(uintptr_t start, size_t blksize)
    : _start(start)
    , _blksize(blksize)
    {}

    void heap::heap_bin::map(void* p, size_t bytes)
    {
        auto addr = reinterpret_cast<uintptr_t>(p);
        if (addr + bytes <= _start) {
            return;
        }

        addr -= _start;

        auto i = addr/_blksize + addr%_blksize;
        auto n = bytes/_blksize + bytes%_blksize;
        if (i > BLOCKS_PER_BIN) {
            return;
        }

        if (i + n > BLOCKS_PER_BIN) {
            n = BLOCKS_PER_BIN - i;
        }

        _bitset.set(i, n);
    }

    bool heap::heap_bin::owns(void* p) const
    {
        return reinterpret_cast<uintptr_t>(p) >= _start 
            && reinterpret_cast<uintptr_t>(p) < _start + blocks_total_size();
    }

    void* heap::heap_bin::allocate(size_t bytes)
    {
        PORPOISE_ASSERT_LESS_OR_EQUAL(bytes, _blksize);
        size_t block;
        if (_bitset.take(block)) {
            return reinterpret_cast<void*>(_start + _blksize*block);
        }

        return OUT_OF_MEMORY;
    }

    void* heap::heap_bin::reallocate(void* p, size_t bytes)
    {
        if (bytes <= _blksize)
        {
            return p;
        }

        return MOVE_BIN;
    }

    void heap::heap_bin::deallocate(void* p)
    {
        auto block = (reinterpret_cast<uintptr_t>(p) - _start)/_blksize;
        _bitset.give_back(block);
    }

    size_t heap::heap_bin::blocks_total_size() const
    {
        return _blksize*_bitset.size();
    }

    size_t heap::heap_bin::total_blocks() const
    {
        return _bitset.size();
    }

    size_t heap::heap_bin::block_size() const
    {
        return _blksize;
    }
    
    size_t heap::heap_bin::bytes_allocated() const
    {
        return _bitset.count()*_blksize;
    }
    
    size_t heap::heap_bin::blocks_allocated() const
    {
        return _bitset.count();
    }
}} // porpoise::mem
