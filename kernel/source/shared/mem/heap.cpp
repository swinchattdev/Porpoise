#include <stdint.h>
#include <stddef.h>

#include <etl/algorithm.h>
#include <etl/array.h>

#define PORPOISE_LOG_GROUP "heap"

#include <porpoise/common.hpp>
#include <porpoise/assert.hpp>
#include <porpoise/io/logging.hpp>
#include <porpoise/lib/sync/lock_guard.hpp>
#include <porpoise/mem/heap.hpp>

using porpoise::lib::sync::make_lock_guard;

extern uint8_t __kernel_end__[];

namespace porpoise { namespace mem {
    heap heap::_inst;

    heap& heap::get(const heap_capability& cap)
    {
        PORPOISE_UNUSED(cap);
        return _inst;
    }

    void heap::init(uintptr_t start, const init_capability& cap)
    {
        PORPOISE_UNUSED(cap);
        auto guard = make_lock_guard(_lock);

        // Ensure start address is page-aligned.
        start = MAX(start, reinterpret_cast<uintptr_t>(__kernel_end__));
        _start = get_aligned_address(start, PAGE_SIZE);
        _end = _start;
        PORPOISE_ASSERT_EQUAL(_start & (PAGE_SIZE - 1), 0UL);

        // Allocate space for each bin and the block array it manages.
        for (size_t i = 0; i < NUM_HEAP_BINS; i++) {
            auto bin_addr = reinterpret_cast<void*>(_end);
            auto bin_blocks_addr = _end + sizeof(heap_bin);

            _bins[i] = new (bin_addr) heap_bin(bin_blocks_addr, BIN_BLOCK_SIZES[i]);
            _end = bin_blocks_addr + _bins[i]->blocks_total_size();
        }

        PORPOISE_ASSERT_GREATER(_end, _start);
        PORPOISE_LOG_DEBUG("Initialised heap of " << (_end - _start) << " B at " << reinterpret_cast<void*>(_start));
    }

    void heap::map(void* p, size_t size)
    {
        PORPOISE_CHECK_UNEQUAL(p, nullptr);
        for (auto bin : _bins)
        {
            bin->map(p, size);
        }

        PORPOISE_LOG_TRACE("Mapped " << size << " B object at " << p);
    }

    void* heap::handle_oom(oom_behaviour behaviour)
    {
        if (behaviour == oom_behaviour::abort)
        {
            PORPOISE_ABORT("Out of memory");
        }

        PORPOISE_LOG_WARN("Returning NULL for OOM event");
        return OUT_OF_MEMORY;
    }

    void* heap::allocate(size_t bytes, oom_behaviour behaviour)
    {
        PORPOISE_CHECK_UNEQUAL(bytes, 0UL);
        auto guard = make_lock_guard(_lock);
        for (auto bin : _bins)
        {
            if (bytes <= bin->block_size())
            {
                auto p = bin->allocate(bytes);
                if (p != OUT_OF_MEMORY)
                {
                    PORPOISE_LOG_TRACE(
                        "Allocated "
                        << bytes
                        << " B at "
                        << p
                        << " using "
                        << bin->block_size()
                        << "B bin"
                    );
                    _allocations++;
                    return p;
                }
            }
        }

        return handle_oom(behaviour);
    }
    
    void* heap::reallocate(void* p, size_t bytes, oom_behaviour behaviour)
    {
        if (p == nullptr)
        {
            return allocate(bytes, behaviour);
        }

        if (bytes == 0)
        {
            deallocate(p);
            return nullptr;
        }

        auto guard = make_lock_guard(_lock);
        for (auto bin : _bins)
        {
            if (!bin->owns(p))
            {
                continue;
            }

            // Bins return nullptr when a resized pointer should move bin. 
            auto q = bin->reallocate(p, bytes);
            if (q)
            {
                PORPOISE_LOG_TRACE(
                    "Resized"
                    << bytes
                    << " B object at "
                    << p
                    << " using "
                    << bin->block_size()
                    << "B bin"
                );

                return q;
            }

            PORPOISE_LOG_TRACE(
                "Moving "
                << bytes
                << " B object at "
                << p
                << " using "
                << bin->block_size()
                << "B bin"
            );

            return allocate(bytes, behaviour);
        }

        return handle_oom(behaviour);
    }

    void heap::deallocate(void* p)
    {
        PORPOISE_CHECK_UNEQUAL(p, nullptr);
        auto guard = make_lock_guard(_lock);
        for (auto bin : _bins)
        {
            if (!bin->owns(p))
            {
                continue;
            }

            bin->deallocate(p);
            _frees++;

            if (_frees > _allocations)
            {
                PORPOISE_LOG_WARN(
                    "Heap has recorded " 
                    << _frees 
                    << " frees and " 
                    << _allocations
                    << " allocations which indicates there is a bug"
                );
            }

            PORPOISE_LOG_TRACE("Freed object at " << p << " from " << bin->block_size() << "B bin)");
            return;
        }

        PORPOISE_LOG_WARN("Got request to free object at " << p << " but it is not owned by any bin");
    }

    void heap::get_statistics(statistics& stats)
    {
        stats.num_bins = _bins.size();
        stats.allocations = _allocations;
        stats.frees = _frees;
        for (auto bin : _bins)
        {
            stats.total_bytes += bin->blocks_total_size();
            stats.bytes_allocated += bin->bytes_allocated();
            stats.total_blocks += bin->total_blocks();
            stats.blocks_allocated += bin->blocks_allocated();
        }
    }
}} // porpoise::mem

void* operator new(size_t size)
{
    PORPOISE_UNUSED(size);
    PORPOISE_ABORT("new called");
}

void* operator new[](size_t size)
{
    PORPOISE_UNUSED(size);
    PORPOISE_ABORT("new[] called");
}

void operator delete(void* p)
{
    PORPOISE_UNUSED(p);
    PORPOISE_ABORT("delete called");
}

void operator delete[](void* p)
{
    PORPOISE_UNUSED(p);
    PORPOISE_ABORT("delete[] called");
}

void operator delete[](void* p, size_t size)
{
    PORPOISE_UNUSED(p);
    PORPOISE_UNUSED(size);
    PORPOISE_ABORT("delete[] called");
}

void operator delete(void* p, size_t size)
{
    PORPOISE_UNUSED(p);
    PORPOISE_UNUSED(size);
    PORPOISE_ABORT("delete called");
}
