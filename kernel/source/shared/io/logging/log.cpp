#include <stdint.h>
#include <stddef.h>

#include <etl/mutex.h>
#include <etl/string.h>

#include <porpoise/common.hpp>
#include <porpoise/lib/sync/lock_guard.hpp>
#include <porpoise/mem/heap.hpp>
#include <porpoise/io/logging.hpp>
#include <porpoise/time/timespan.hpp>


namespace porpoise { namespace io { namespace logging {
    using namespace time;
    using porpoise::lib::sync::make_lock_guard;

    log_level Log::_minimum_level = static_cast<log_level>(CONFIG_LOG_MIN_LEVEL);
    log_sink* Log::_sinks[MAX_SINK];
    int Log::_num_sinks;
    etl::mutex Log::_sink_lock;

    Log Log::get(log_level level, const char* lvlstr)
    {
        _sink_lock.lock();

        Log inst(level);
        auto millis = timespan::read_timestamp_counter().millis();
        auto secs   = millis/1000;
        millis -= secs*1000;
        inst << set_width(4) << set_fill('0') << secs << reset()
             << '.' 
             << set_width(3) << set_fill('0') << millis << reset() 
             << ' '
             << lvlstr 
             << "] "
        ;
        return inst;
    }

    Log Log::trace()
    {
        return get(log_level::trace, "trace");
    }

    Log Log::debug()
    {
        return get(log_level::debug, "debug");
    }

    Log Log::info()
    {
        return get(log_level::info, "info");
    }

    Log Log::warn()
    {
        return get(log_level::warn, "warn");
    }

    Log Log::error()
    {
        return get(log_level::error, "error");
    }

    bool Log::add_sink(log_sink* sink)
    {
        auto guard = make_lock_guard(_sink_lock);
        if (sink == nullptr)
        {
            return false;
        }

        if (_num_sinks >= MAX_SINK)
        {
            return false;
        }

        _sinks[_num_sinks++] = sink;
        return true;
    }

    void Log::minimum_level(log_level next)
    {
        if (static_cast<int>(next) > static_cast<int>(log_level::warn)) {
            // Do not allow suppression of warnings or errors.
            _minimum_level = log_level::warn;
        }

        _minimum_level = next;
    }

    log_level Log::minimum_level()
    {
        return _minimum_level;
    }

    int Log::num_sinks()
    {
        return _num_sinks;
    }

    Log::Log(log_level level)
    : _current_level(level)
    , _field_width(0)
    , _fill_char(' ')
    , _base(10)
    , _prefix(false)
    , _boolalpha(false)
    , _hexupper(false)
    , _is_active(true)
    {}

    Log::Log(Log&& other)
    {
        _current_level = other._current_level;
        _field_width = other._field_width;
        _fill_char = other._fill_char;
        _base = other._base;
        _prefix = other._prefix;
        _boolalpha = other._boolalpha;
        _hexupper = other._hexupper;
        _is_active = true;
        other._is_active = false;
    }

    void Log::operator=(Log&& other)
    {
        _current_level = other._current_level;
        _field_width = other._field_width;
        _fill_char = other._fill_char;
        _base = other._base;
        _prefix = other._prefix;
        _boolalpha = other._boolalpha;
        _hexupper = other._hexupper;
        _is_active = true;
        other._is_active = false;
    }

    bool Log::is_active_instance() const
    {
        return _is_active;
    }

    Log::~Log()
    {
        if (is_active_instance())
        {
            internal_emit("\r\n");
            _sink_lock.unlock();
        }
    }

    void Log::internal_emit(const char* s)
    {
        for (auto i = 0; i < _num_sinks; i++)
        {
            _sinks[i]->emit(_current_level, s);
        }
    }

    void Log::internal_emit(char c)
    {
        for (auto i = 0; i < _num_sinks; i++)
        {
            _sinks[i]->emit(_current_level, c);
        }
    }

    void Log::emit(char c)
    {
        if (!is_active_instance() || _current_level < _minimum_level)
        {
            return;
        }

        for (auto j = 1; j < _field_width; j++)
        {
            internal_emit(_fill_char);
        }

        internal_emit(c);
    }

    void Log::emit(const char* string)
    {
        if (!is_active_instance() || _current_level < _minimum_level)
        {
            return;
        }

        for (auto j = 1; j < _field_width; j++)
        {
            internal_emit(_fill_char);
        }

        internal_emit(string);
    }

    void Log::emit(intmax_t number)
    {
        if (!is_active_instance() || _current_level < _minimum_level)
        {
            return;
        }

        if (number < 0)
        {
            internal_emit('-');
            emit(static_cast<uintmax_t>(-number));
        }
        else
        {
            emit(static_cast<uintmax_t>(number));
        }
    }

    void Log::emit(uintmax_t number)
    {
        if (!is_active_instance() || _current_level < _minimum_level)
        {
            return;
        }

        static constexpr unsigned DIGIT_MAX = sizeof(uintmax_t)*CHAR_BIT;
        char buffer[DIGIT_MAX];
        auto p     = buffer;
        auto q     = buffer + DIGIT_MAX;
        auto alpha = _hexupper ? 'A' : 'a';
        do
        {
            auto r = number%_base;
            if (r < 10)
            {
                *p++ = '0' + r;
            }
            else
            {
                *p++ = alpha + r - 10;
            }

            number /= _base;
        } while (number && p < q);

        auto count = p - buffer;
        if (_prefix)
        {
            switch (_base)
            {
                case 2:  internal_emit("0b"); count += 2; break;
                case 8:  internal_emit("0");  count += 1; break;
                case 16: internal_emit("0x"); count += 2; break;
                default: break;
            }
        }

        for (; count < _field_width; count++)
        {
            internal_emit(_fill_char);
        }

        while (p-- > buffer)
        {
            internal_emit(*p);
        }
    }

    uint8_t Log::field_width() const
    {
        if (!is_active_instance())
        {
            return 0;
        }

        return _field_width;
    }

    char Log::fill_char() const
    {
        if (!is_active_instance())
        {
            return 0;
        }

        return _fill_char;
    }

    uint8_t Log::base() const
    {
        if (!is_active_instance())
        {
            return 0;
        }

        return _base;
    }

    bool Log::prefix() const
    {
        if (!is_active_instance())
        {
            return false;
        }

        return _prefix;
    }

    bool Log::boolalpha() const
    {
        if (!is_active_instance())
        {
            return false;
        }

        return _boolalpha;
    }

    bool Log::hexupper() const
    {
        if (!is_active_instance())
        {
            return false;
        }

        return _hexupper;
    }

    void Log::field_width(uint8_t next)
    {
        if (!is_active_instance())
        {
            return;
        }

        _field_width = next;
    }

    void Log::fill_char(char next)
    {
        if (!is_active_instance())
        {
            return;
        }

        _fill_char = next;
    }

    void Log::base(uint8_t next)
    {
        if (!is_active_instance())
        {
            return;
        }

        if (next < 2)
        {
            return;
        }

        _base = next;
    }

    void Log::prefix(bool next)
    {
        if (!is_active_instance())
        {
            return;
        }

        _prefix = next;
    }

    void Log::boolalpha(bool next)
    {
        if (!is_active_instance())
        {
            return;
        }

        _boolalpha = next;
    }

    void Log::hexupper(bool next)
    {
        if (!is_active_instance())
        {
            return;
        }

        _hexupper = next;
    }
}}} // porpoise::io::logging
