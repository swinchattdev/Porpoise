#include <stdint.h>

#include <etl/algorithm.h>
#include <etl/atomic.h>

#define PORPOISE_LOG_GROUP "boot"

#include <porpoise/boot.hpp>
#include <porpoise/common.hpp>
#include <porpoise/capability.hpp>
#include <porpoise/mem/heap.hpp>
#include <porpoise/io/logging.hpp>
#include <porpoise/io/logging/sinks/serial-sink.hpp>
#include <porpoise/io/uart.hpp>
#include <porpoise/time/timespan.hpp>
#include <porpoise/time/delay.hpp>

#include "./internal/boot-internal.hpp"

using namespace porpoise::boot::internal;
using namespace porpoise::io;
using namespace porpoise::io::logging;
using namespace porpoise::time;

extern "C" void _start();
extern "C" void _init();
extern "C" void _fini();

extern "C" uint8_t __bss_start__[];
extern "C" uint8_t __bss_end__[];
extern "C" uint8_t __kernel_end__[];

namespace porpoise {
    static struct init_capability {} __init_capability;
    static struct heap_capability {} __heap_capability;
}

namespace porpoise { namespace boot {
    static etl::atomic<bool> cpu0_init_complete(false);
    static etl::atomic<int> cpus_started(0);

    static void clear_bss()
    {
        etl::fill(&__bss_start__[0], &__bss_end__[0], static_cast<uint8_t>(0));
    }

    static void initialise_binary_environment()
    {
        clear_bss();
        _init();
    }

    static void initialise_heap(void* dtb_address)
    {
        auto& heap = mem::heap::get(__heap_capability);
        heap.init(reinterpret_cast<uintptr_t>(__kernel_end__), __init_capability);

        // Map the DTB if present.
        if (dtb_address)
        {
            heap.map(dtb_address, 0x1000);
        }

        perform_heap_sanity_checks(heap);
    }


    static void initialise_logging()
    {
        static uart* uart0 = uart::init();
        static serial_sink uart0_sink(uart0, log_level::trace);

        Log::add_sink(&uart0_sink);

        PORPOISE_LOG_DEBUG("CPU 0 started, performing basic initialization");
    }

    // Ensure the other CPUs run.
    static void initialise_smp()
    {
#if CONFIG_SMP_ENABLED && CONFIG_SMP_CPU_COUNT > 1
        PORPOISE_LOG_DEBUG("CPU 0 initialization complete, waking up " << CONFIG_SMP_CPU_COUNT - 1 << " remaining CPUs");
        for (auto i = 1; i < CONFIG_SMP_CPU_COUNT; i++) {
            start_cpu(i, _start);
        }
        PORPOISE_LOG_DEBUG("CPU 0 entering cpu_main");
#else
        PORPOISE_LOG_DEBUG("CPU 0 initialization complete, entering cpu_main [SMP disabled]");
#endif
    }

    // CPU 0 brings up subsystems.
    static void start_cpu_0(void* dtb_address)
    {
        initialise_binary_environment();
        cpus_started++;
        initialise_logging();
        initialise_heap(dtb_address);
        cpu0_init_complete.store(true);
        initialise_smp();
        cpu_main(0, __heap_capability);
        _fini();
    }

    // Remaining CPUs wait for CPU0 to finish initialising.
    static void start_cpu(int id)
    {
        PORPOISE_ASSERT_UNEQUAL(id, 0);
        cpus_started++;

        // Wait for CPU 0 initialisation.
        while (!cpu0_init_complete.load())
        {
            delay(timespan::millis(1));
        }

        cpu_main(id, __heap_capability);
    }

    extern "C" void boot_kernel(uint64_t dtb_address, uint64_t id)
    {
        if (id == 0)
        {
            dtb_address &= 0xFFFFFFFF; // Clear upper 32 bits.
            start_cpu_0(reinterpret_cast<void*>(dtb_address));
        }
        else
        {
            start_cpu(static_cast<int>(id));
        }

        while (true)
            ;
    }
}} // porpoise::boot
