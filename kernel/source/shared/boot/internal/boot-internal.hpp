#pragma once

#include <porpoise/mem/heap.hpp>

namespace porpoise { namespace boot { namespace internal {
    void perform_heap_sanity_checks(mem::heap& heap);
}}} // porpoise::boot::internal
