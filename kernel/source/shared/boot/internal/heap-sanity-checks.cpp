#include <stdint.h>

#define PORPOISE_LOG_GROUP "boot"

#include <porpoise/boot.hpp>
#include <porpoise/common.hpp>
#include <porpoise/mem/heap.hpp>
#include <porpoise/io/logging.hpp>
#include <porpoise/time/timespan.hpp>

using namespace porpoise::io::logging;
using namespace porpoise::time;

namespace porpoise { namespace boot { namespace internal {
    struct heap_check_results {
        size_t allocate_checks = 0;
        size_t deallocate_checks = 0;
        uintmax_t allocate_cycles = 0;
        uintmax_t deallocate_cycles = 0;

        void trace()
        {
            auto hertz = timespan::cpu_hertz();
            auto allocate_latency_cyc = allocate_cycles/allocate_checks;
            auto deallocate_latency_cyc = deallocate_cycles/deallocate_checks;
            auto allocate_latency_us = CYCLES_TO_MICROS(allocate_latency_cyc, hertz);
            auto deallocate_latency_us = CYCLES_TO_MICROS(deallocate_latency_cyc, hertz);

            PORPOISE_LOG_DEBUG(
                "Heap performance during checks:\n"
                << "Total allocations      : " << allocate_checks << "\n"
                << "  Avg latency (cyc/us) : " << allocate_latency_cyc << " / " << allocate_latency_us << "\n"
                << "Total deallocations    : " << deallocate_checks << "\n"
                << "  Avg latency (cyc/us) : " << deallocate_latency_cyc << " / " << deallocate_latency_us
            );
        }
    };

    static bool is_ptr_null(void* ptr)
    {
        if (ptr == nullptr)
        {
            PORPOISE_LOG_ERROR("Heap returned null pointer");
            return true;
        }

        return false;
    }

    static bool is_ptr_unaligned(void* ptr)
    {
        uintptr_t addr = reinterpret_cast<uintptr_t>(ptr);
        if (mem::is_address_aligned(addr, sizeof(uint64_t)))
        {
            return false;
        }

        PORPOISE_LOG_ERROR("Heap returned misaligned address: " << ptr);
        return true;
    }

    static size_t check_heap_ptrs(mem::heap& heap, size_t blksize, heap_check_results& results)
    {
        size_t checks_failed = 0;
        blksize -= !(blksize%2); // Heap should return aligned addresses even if block sizes are odd.

        void* p;
        void* q;

        {
            auto start = timespan::read_timestamp_counter();
            p = heap.allocate(blksize, mem::oom_behaviour::abort);
            auto end = timespan::read_timestamp_counter();
            results.allocate_checks++;
            results.allocate_cycles += (end - start).cycles();
        }

        {
            auto start = timespan::read_timestamp_counter();
            q = heap.allocate(blksize, mem::oom_behaviour::abort);
            auto end = timespan::read_timestamp_counter();
            results.allocate_checks++;
            results.allocate_cycles += (end - start).cycles();
        }

        if (is_ptr_null(p))
        {
            checks_failed++;
        }
        else if (is_ptr_unaligned(p))
        {
            checks_failed++;
        }
        else
        {
            etl::fill(reinterpret_cast<uint8_t*>(p), reinterpret_cast<uint8_t*>(p) + blksize, static_cast<uint8_t>(0));
        }

        if (is_ptr_null(q))
        {
            checks_failed++;
        }
        else if (is_ptr_unaligned(q))
        {
            checks_failed++;
        }
        else
        {
            etl::fill(reinterpret_cast<uint8_t*>(q), reinterpret_cast<uint8_t*>(q) + blksize, static_cast<uint8_t>(0));
        }

        {
            auto start = timespan::read_timestamp_counter();
            heap.deallocate(p);
            auto end = timespan::read_timestamp_counter();
            results.deallocate_checks++;
            results.deallocate_cycles += (end - start).cycles();
        }

        {
            auto start = timespan::read_timestamp_counter();
            heap.deallocate(q);
            auto end = timespan::read_timestamp_counter();
            results.deallocate_checks++;
            results.deallocate_cycles += (end - start).cycles();
        }

        return checks_failed;
    }

    void perform_heap_sanity_checks(mem::heap& heap)
    {
        // Sanity check the heap's behaviour.
        // * It should not return nullptrs (with oom_behaviour::abort)
        // * It should not return a pointer that is already allocated
        // * The addresses returned should be aligned to 8 byte boundaries
        PORPOISE_LOG_DEBUG("Starting heap sanity checks");
        size_t checks_failed = 0;
        heap_check_results results;
        for (size_t i = 0; i < mem::heap::NUM_HEAP_BINS; i++)
        {
            checks_failed += check_heap_ptrs(heap, mem::heap::BIN_BLOCK_SIZES[i], results);
        }

        if (checks_failed > 0)
        {
            PORPOISE_ABORT("Heap failed " << checks_failed << " sanity checks");
        }
        else
        {
            PORPOISE_LOG_DEBUG("Heap passed all sanity checks");
            results.trace();
        }
    }
}}} // porpoise::boot::internal
