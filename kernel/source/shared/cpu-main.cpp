#define PORPOISE_LOG_GROUP "main"

#include <porpoise/common.hpp>
#include <porpoise/boot.hpp>
#include <porpoise/capability.hpp>
#include <porpoise/io/logging.hpp>
#include <porpoise/time/delay.hpp>
#include <porpoise/time/timespan.hpp>

using namespace porpoise::io::logging;
using porpoise::time::delay;
using porpoise::time::timespan;

namespace porpoise {
    void cpu_main(int id, const heap_capability& heap_cap)
    {
        PORPOISE_UNUSED(heap_cap);
        PORPOISE_LOG_INFO("Hello from CPU " << id << " (" << time::timespan::cpu_hertz()/1000000 << "MHz)");
    }
} // porpoise
