#!/usr/bin/env python3

'''Usage: {0} <config.ini> [printer] [eol]

    <printer> is one of the following: {1} (default is {2})
    
    <eol> is a string to separate lines (default is {3})

NB: if a second arg is given and it is not a valid printer, it will be presumed to be <eol> and any further args will be
ignored.'''

import os
import sys

NAME = os.path.basename(sys.argv[0])
DEFAULT_PRINTER = 'cflags'
DEFAULT_EOL = ';'

def cflags_printer(sec, key, val, eol):
    if sec.upper() == 'MACROS':
        print('-D{}={}'.format(key, val), end=eol)
    else:
        print('-DCONFIG{}_{}={}'.format(('_' + sec.upper()) if sec else '', key.upper(), val), end=eol)

def cmake_printer(sec, key, val, eol):
    if sec.upper() == 'MACROS':
        print('{}={}'.format(key, val), end=eol)
    else:
        print('CONFIG{}_{}={}'.format(('_' + sec.upper()) if sec else '', key.upper(), val), end=eol)

PRINTERS = {
    'cflags': cflags_printer,
    'cmake': cmake_printer
}

def cat_config(filename, printer, eol):
    with open(filename) as ini:
        section = ''
        line_num = 0
        for raw_line in ini:
            line_num += 1
            line = raw_line.strip()
            if not line or line[0] == '#':
                # Skip blank/comment line.
                continue
            if line[0] == '[':
                # Parse section name.
                pos = line.find(']')
                if pos < 0:
                    raise ValueError(f'{filename}:{line_num}: Unmatched [ on section line')
                section = line[1:pos]
            else:
                # Split key=value pair over equals sign. Strip any comment & leading and trailing whitespace.
                elems = line.split('=')
                if len(elems) != 2:
                    raise ValueError(f'{filename}:{line_num}: Incorrect number of equals signs (expected 1)')
                pos = elems[1].find('#')
                if pos >= 0:
                    elems[1] = elems[1][0:pos]
                elems[0] = elems[0].strip()
                elems[1] = elems[1].strip()
                printer(section, elems[0], elems[1], eol)

if __name__ == '__main__':
    if len(sys.argv) < 2 or sys.argv[1] in ('-h','-help','--help'):
        print(
            __doc__.format(
                sys.argv[0], 
                ', '.join(PRINTERS.keys()), 
                DEFAULT_PRINTER, 
                DEFAULT_EOL
            ), 
            file=sys.stderr
        )
        sys.exit(1)

    argi = 1

    filename = sys.argv[argi]
    if not os.path.exists(filename):
        print(f'{NAME}: {filename} not found', file=sys.stderr)
        sys.exit(1)
    argi += 1

    printer = DEFAULT_PRINTER
    if len(sys.argv) > 2:
        printer = sys.argv[argi]
        argi += 1
    if printer not in PRINTERS:
        printer = DEFAULT_PRINTER
        argi -= 1

    eol = DEFAULT_EOL
    if len(sys.argv) > 3:
        eol = sys.argv[argi]
        argi += 1
    if eol[0] == '\\':
        eol = {
            'n': '\n',
            's': ' ',
            't': '\t'
        }[eol[1]] + eol[2:]

    cat_config(filename, PRINTERS[printer], eol)
