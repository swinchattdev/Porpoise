#!/bin/bash

register_subcommand "help"
register_subcommand_alias "h" "help"
register_subcommand_alias "-h" "help"
register_subcommand_alias "-help" "help"
register_subcommand_alias "--help" "help"

function _help_indent {
    local levels=$1
    while read line; do
        local i=0
        while [[ $i -lt $levels ]]; do
            echo -en "\t"
            i=$[i + 1]
        done
        echo -e "$line"
    done
}

function help_usage {
    echo "[command]"
}

function help_help {
    cat <<EOF
Show general help or, if specified, help for the sub-command <command>. Afterwards, quit.
EOF
}

function _help_aliases {
    local subcommand="$1"
    if [[ "$(echo "${PORPOISE_COMMAND_ALIASES[@]}" | grep "$subcommand")" != "" ]]; then
        echo -n "Aliases for $subcommand: "
        local alias
        for alias in ${!PORPOISE_COMMAND_ALIASES[@]}; do
            [[ "${PORPOISE_COMMAND_ALIASES["$alias"]}" = "$subcommand" ]] && echo -n "$alias "
        done
        echo
    fi
}

function help_command {
    local subcommand="$1"
    local indent="$2"

    if [[ -z "$subcommand" ]]; then
        echo "Usage: $0 <command> [command options]"
        echo
        echo "Available commands:"
        local cmd
        for cmd in ${PORPOISE_COMMANDS[@]}; do
            echo -e "\t$cmd $(eval "${cmd}_usage")"
            help_command "$cmd" 2
            echo
        done
        return
    fi

    subcommand=$(command_or_alias "$subcommand")

    if [[ $(echo "${PORPOISE_COMMANDS[@]}" | grep "$subcommand") = "" ]]; then
        help_command >&2
        die "Invalid subcommand: \"$subcommand\""
    fi

    if [[ -z "$indent" ]]; then
        echo "Usage: $0 $subcommand $(eval "${subcommand}_usage")"
        echo
        _help_aliases "$subcommand"
        echo
        eval "${subcommand}_help"
    else
        (
            _help_aliases "$subcommand"
            eval "${subcommand}_help" 
        ) | _help_indent $indent
    fi
}

function show_help {
    help_command "$@"
}
