cmake_minimum_required(VERSION 3.20)

macro (add_subdirectory_if_exists dir)
    if (IS_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/${dir})
        add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/${dir})
    endif (IS_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/${dir})
endmacro (add_subdirectory_if_exists dir)
