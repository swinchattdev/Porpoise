#!/usr/bin/env python3

'''Usage: {0} <machines.csv>

Read machine configuration from machines.csv and output flags for the CMAKE_MACHINE_ARGS array in the porpoise script.'''

import csv
import os
import sys

PROGRAM_NAME = os.path.basename(sys.argv[0])

class Machine:
    '''Description of a machine.'''
    _machines = {}

    @classmethod
    def _bad_record(cls, filename, line_number):
        print(f'{PROGRAM_NAME}: Bad record at {filename}:{line_number}', file=sys.stderr)
        sys.exit(1)

    @classmethod
    def load_all_from_csv(cls, filename):
        '''Load all machines from a CSV file and return a dictionary mapping the name+version to the machine instance.'''
        aliases = {}
        with open(filename, newline='') as csvfile:
            line_number = 0
            for record in csv.reader(csvfile, delimiter='\t'):
                try:
                    index = 0
                    machine = record[index]; index += 1
                    version = record[index]; index += 1
                    target = record[index]; index += 1
                    if target.startswith('$'):
                        aliases[f'{machine}{version}'] = target[1:]
                        continue

                    cflags = record[index]; index += 1

                    qemu_bin = ''
                    qemu_args = ''
                    if len(record) > index:
                        qemu_bin = record[index]; index += 1
                        if len(record) > index:
                            qemu_args = record[index]; index += 1
                    cls._machines[f'{machine}{version}'] = Machine(machine, version, target, cflags, qemu_bin, qemu_args)
                except:
                    cls._bad_record(filename, line_number)
                line_number += 1
        for alias, target in aliases.items():
            cls._machines[alias] = cls._machines[target]
        return cls._machines

    def __init__(self, machine, version, target, cflags, qemu_bin, qemu_args):
        self.machine = machine
        self.version = version
        self.target = target
        self.cflags = cflags
        self.qemu_bin = qemu_bin
        self.qemu_args = qemu_args

    @property
    def macros(self):
        return ' '.join(
            [
                f'-DMACHINE:STRING={self.machine}',
                f'-DMACHINE_VERSION:STRING={self.version}',
                f'-DMACHINE_TARGET_TRIPLE={self.target}',
                f'-DMACHINE_CMDLINE_DEFINES={self.cflags}'
            ]
        )

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print(__doc__.format(sys.argv[0]), file=sys.stderr)
        sys.exit(1)
    machines = Machine.load_all_from_csv(sys.argv[1])
    for name, machine in machines.items():
        print(f'{name};{machine.macros};{machine.qemu_bin};{machine.qemu_args}')
